﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;

namespace ConsoleAppTest
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //Process();

            //Console.Read();


            Action<int, int> A1 = (int a, int b) => { 
                Console.WriteLine(a+b);
            };

            Func<int, int> F1 = (int a) =>
            {
                return a;
            };
            A1.Invoke(2,67);
            Console.WriteLine(F1.Invoke(10));

            Console.WriteLine(default(int));
        }

        private static async void Process()
        {
            string token = GetSecurityToken("zzhi", "12345", "http://localhost:45690/api/Account", ".ASPXAUTH");
            string address = "http://localhost:45690/api/products";
            if (!string.IsNullOrEmpty(token))
            {
                HttpClientHandler handler = new HttpClientHandler {CookieContainer = new CookieContainer()};
                handler.CookieContainer.Add(new Uri(address), new Cookie(".ASPXAUTH", token));
                using (HttpClient httpClient = new HttpClient(handler))
                {
                    HttpResponseMessage response = httpClient.GetAsync(address).Result;
                    IEnumerable<Product> Products = await  response.Content.ReadAsAsync<IEnumerable<Product>>();
                  
                    foreach (Product c in Products)
                    {
                        Console.WriteLine(c.Name);
                    }
                }

            }
        }


        private static string GetSecurityToken(string userName, string password, string url, string cookieName)
        {

            using (HttpClient httpClient = new HttpClient())
            {
                Dictionary<string, string> credential = new Dictionary<string, string>();
                credential.Add("Username", userName);
                credential.Add("Password", password);
                HttpResponseMessage response = httpClient.PostAsync(url, new FormUrlEncodedContent(credential)).Result;
                IEnumerable<string> cookies;
                if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                {
                    string token = cookies.FirstOrDefault(value => value.StartsWith(cookieName));
                    if (null == token)
                    {
                        return null;
                    }
                    return token.Split(';')[0].Substring(cookieName.Length + 1);
                }
                return null;
            }
        }
    }
}
